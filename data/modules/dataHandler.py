import json
import os
import pandas as pd
from dotenv import load_dotenv
from supabase import create_client, Client

load_dotenv()
url: str = os.environ.get("SUPABASE_URL")
key: str = os.environ.get("SUPABASE_KEY")
supabase: Client = create_client(url, key)


data = []
commits_data = []
commit_files_data = []

with open('../mock/commits.json', 'r') as f:
    data = json.load(f)

df = pd.DataFrame(data)

df_commits = df[
    [
        'sha', 'author_name', 'author_email', 'authored_date', 'message',
        'project_name', 'project_username', 'project_provider', 'project_org', 'project_url',
        'total_insertions', 'total_deletions', 'total_lines', 'total_files'
    ]
].drop_duplicates()
df_commits['authored_date'] = pd.to_datetime(df_commits['authored_date'], unit='s')
df_commits['authored_date'] = df_commits['authored_date'].dt.strftime('%Y-%m-%d %H:%M:%S')
df_commits.rename(columns={'sha': 'id'}, inplace=True)
df_commit_files = df[['sha', 'file_name', 'file_insertions', 'file_deletions', 'file_lines']]
df_commit_files.rename(columns={'sha': 'commit_id'}, inplace=True)

commits_table = 'commits'
commit_files_table = 'commit_files'

print('Enviando a commits')
for _, row in df_commits.iterrows():
    commit_info = row.to_dict()
    supabase.table(commits_table).insert(commit_info).execute()

print('Enviando a commit_files')
for _, row in df_commit_files.iterrows():
    file_info = row.to_dict()
    supabase.table(commit_files_table).insert(file_info).execute()

print('tuki')
