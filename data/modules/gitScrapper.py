import os
from typing import Optional
from git import Repo
from git.util import rmtree


class RepositoryAttrs:
    username: str
    org: Optional[str] = None
    folders: list[str] = []
    name: str
    provider: str
    url: str
    url_formatted: str


class URLFormatError(Exception):
    def __init__(self, message):
        super().__init__(message)


class GitScrapper:
    def __init__(self, tmp_folder: str = '.\\tmp', repository_url: str = ''):
        if not repository_url:
            raise URLFormatError(message='The URL must not be empty!')

        self.tmp_folder = tmp_folder
        self.repository_attrs = self._repository_attrs(url=repository_url)

    @staticmethod
    def _repository_attrs(url: str) -> RepositoryAttrs:
        """
        Given an url of repository, returns attributes.
        The url must have `https://` at the beginning, otherwise raise error.
        """
        protocol = 'https://'

        if not url.startswith(protocol):
            raise URLFormatError(message=f'The url of repository must have {protocol} at the beginning!')

        repo_attrs = RepositoryAttrs()
        url_formatted = url.lstrip(protocol).rstrip('/')
        repo_attrs.url_formatted = url_formatted
        repo_attrs.url = url

        url_formatted = url_formatted.split('/')
        repo_attrs.provider = url_formatted[0]
        repo_attrs.username = url_formatted[1]
        repo_attrs.name = url_formatted[-1]

        if len(url_formatted) > 3:
            repo_attrs.org = url_formatted[2]

        if len(url_formatted) > 4:
            repo_attrs.folders = url_formatted[3:-1]

        return repo_attrs

    def commits(self):
        """
        Scrap commits from repository
        """
        # If tmp folder exists, remove folder and all content
        if os.path.isdir(self.tmp_folder):
            rmtree(self.tmp_folder)

        repository_folder = os.path.join(
            self.tmp_folder,
            self.repository_attrs.username,
            self.repository_attrs.name
        )
        repo = Repo.clone_from(self.repository_attrs.url, repository_folder)
        commits_by_file = [
            {
                'sha': commit.hexsha,
                'author_name': commit.author.name,
                'author_email': commit.author.email,
                'authored_date': commit.authored_date,
                'message': commit.message,
                'project_name': self.repository_attrs.name,
                'project_username': self.repository_attrs.username,
                'project_provider': self.repository_attrs.provider,
                'project_org': self.repository_attrs.org,
                'project_url': self.repository_attrs.url,
                'total_insertions': commit.stats.total.get('insertions'),
                'total_deletions': commit.stats.total.get('deletions'),
                'total_lines': commit.stats.total.get('lines'),
                'total_files': commit.stats.total.get('files'),
                'file_name': file,
                'file_insertions': stats_file.get('insertions'),
                'file_deletions': stats_file.get('deletions'),
                'file_lines': stats_file.get('lines')
            }
            for commit in repo.iter_commits()
            for file, stats_file in commit.stats.files.items()
        ]
        return commits_by_file
