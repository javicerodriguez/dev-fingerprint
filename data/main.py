import sys
import json
from modules.gitScrapper import GitScrapper


class InputError(Exception):
    def __init__(self, message):
        super().__init__(message)


def init(url: str):
    git = GitScrapper(repository_url=url)
    commits = git.commits()
    with open('mock/commits.json', 'w') as fout:
        json.dump(commits, fout)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        raise InputError(message='The repository url was not provided!')

    if len(sys.argv) > 2:
        raise InputError(message='More arguments were entered than expected! (must only be the url of the repository)')

    [_, repository_url] = sys.argv
    init(repository_url)
